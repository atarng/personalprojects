using System;

public interface CombatInterface
{
	object SearchForTarget();
	void TakeDamage(int amount);
}
