using System;
using UnityEditor;
using UnityEngine;

public class GameUnit :
	MonoBehaviour
	, CombatInterface
	, MovementInterface
{
	int             m_iUnitHealth;
    protected float m_fAcceleration;
    protected float m_fRotation;
    
    /*
	Vector3 m_vVelocity;
	Vector3 m_vAcceleration;
	*/
//// MonoBehavior
	public void Start()
	{
		m_iUnitHealth   = 0;
        /*
		m_vVelocity     = Vector3.zero;
		m_vAcceleration = Vector3.zero;
        */      
	}
	
//// CombatInterfaceImplementation
	public void TakeDamage(int amount)
	{
		m_iUnitHealth -= amount;
	}
	public object SearchForTarget()
	{
		return this;
	}
//// MovementInterfaceImplementation
	public void ProcessMovement()
	{
//        transform.Translate(transform.forward * m_fAcceleration);
        transform.position += (transform.forward * m_fAcceleration);
        transform.Rotate(0,m_fRotation,0);
		/*
        m_vVelocity        += m_vAcceleration;
		transform.position += m_vVelocity;
        */      
	}
}