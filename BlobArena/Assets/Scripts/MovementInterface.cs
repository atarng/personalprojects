﻿using UnityEngine;
using System.Collections;

public interface MovementInterface
{
	void ProcessMovement();
}