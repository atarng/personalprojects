﻿using UnityEngine;
using System.Collections;

public class AttackZone : MonoBehaviour
{
    Collider m_collider;
	// Use this for initialization
	void Start()
    {
        m_collider = GetComponent<Collider>();
        if(m_collider == null)
        {
            Debug.LogWarning("Attack Created without collider");
        }
	}
	/* Update is called once per frame
	void Update()
    {

	}
    */
    void OnTriggerEnter()
    {
        Destroy(gameObject);
    }
}