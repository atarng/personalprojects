﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BasePlayer :
  GameUnit
{
    List<Ability> m_abilityList;

	void Awake()
	{
		SingletonManager.Player = gameObject;
	}
	void Start()
	{

	}
	void Update()
	{
		ProcessInput();
        ProcessMovement();

        Transform myModel = transform.FindChild("StandInModel");
        myModel.transform.localRotation = Quaternion.identity;

        Debug.DrawLine(transform.position, transform.position + (transform.forward * 5), Color.red);
        
//        transform.rotation = myModel.transform.rotation;
	}

	void ProcessInput()
	{
        InputHelper im = InputHelper.Instance();
        /*
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        transform.Translate(x,0,z);
        */
        m_fAcceleration = Input.GetAxis("Vertical");
        m_fRotation     = Input.GetAxis("Horizontal");
	}

    void UseAbility(int index)
    {
        m_abilityList[index].UseAbility();
    }
}