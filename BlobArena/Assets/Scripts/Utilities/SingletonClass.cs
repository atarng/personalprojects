using System;
using UnityEngine;
using System.Reflection;
public class SingletonClass <T> where T : class
{
	static T m_instance;
	public SingletonClass(){}

	public static T Instance()
	{
		if(m_instance == null)
		{
//			m_instance = new T();
			CreateInstance();
		}
		return m_instance;
	}
	static void CreateInstance()
	{
		Type t = typeof(T);
		ConstructorInfo[] ctors = t.GetConstructors ();
		if (ctors.Length > 0)
		{
			throw new InvalidOperationException(string.Format("{0} has an accessible constructor and thus is not a singleton.", t.Name));
		}
		m_instance = (T) Activator.CreateInstance(t,true);
	}
}