﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
	public enum CameraType
	{
		eNone     = 0
	  , eFreeCam  = 1
      , eFixedCam = 2
	}

    public CameraType m_type;
    GameObject        m_playerRef;

    Vector3 m_playerOffset = new Vector3(0,5,0);

	// Use this for initialization
	void Start()
	{
        m_playerRef = GameObject.FindGameObjectWithTag("Player");

	}
	// Update is called once per frame
	void Update()
	{
        if( m_type == CameraType.eFreeCam )
        {
            //Grab input and apply to camera.
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");
            
            transform.Translate(x,y,0f);
        }
        else if( m_type == CameraType.eFixedCam )
        {
            if(m_playerRef != null)
            {
                transform.position = m_playerRef.transform.position - ((m_playerRef.transform.forward * 5)) + m_playerOffset;
                transform.LookAt(m_playerRef.transform.position);
//                float angleDifference= Vector3.Angle(-m_playerRef.transform.forward, transform.position );
//                transform.RotateAround(m_playerRef.transform.position, Vector3.up, angleDifference);
            }
        }
	}
}