using System;
using UnityEngine;

public class SingletonManager : MonoBehaviour
{
    static GameObject playerRef;
    public static GameObject Player
    {
        get{ return playerRef; }
        set
        {
            playerRef = (value.GetComponent<BasePlayer>() != null) ? value : null;
        }
    }

	void Update()
	{
		InputHelper.Instance().Update();
	}
}